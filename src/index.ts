import { Tools } from "./Tools";
module.exports.Tools = Tools;

import * as Vue from "./Vue";
module.exports.Vue = Vue.default;

export default {
  Tools,
  Vue,
};
